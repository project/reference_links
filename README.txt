/* $Id:  */

-- SUMMARY --

"Reference Links" allows custom Urls to be attached to nodes. Its use is seen basically for documentation pages
on a site.

This module allows users to add links (internal or external) to nodes, that can provide further information about
the current node. The important thing is that Users need NOT have edit permissions for the node. You can allow
users with non-edit permissions to add/attach new links to nodes, when the page is being viewed normally.

Moreover, authorized users can also rate the links (this module uses Voting API for the rating part),
and links are always shown in decreasing order of their average rating.
All actions are performed via Ajax, and no page refresh is required.

If I try to summarize it, this module can provide a References/See also/Bibliography kind of links for nodes.
You can control which content types you want the urls to be attached to.

Starting with version 6.x-1.2, the module now supports configurable Voting widgets.
You can currently choose between a Star Rating widget (similar to the 5-star module),
or an Up/Down Rating widget (similar to the 1-Plus module).
More configuration options like allowing users to change votes is also supported now.

Credits to Saeven Alexandre of saeven.net for allowing me to use his ExtJs Rating extension for this module
under GPL 2 license.

-- REQUIREMENTS --

1) ExtJs Javascript library
  Reference Links module uses ExtJs javascript library and is currently based on ExtJs 3.1.0
  (although it should work on immediately preceding or succeeding ExtJs releases).
  Download the copy of ExtJs from:
  http://www.extjs.com/products/extjs/download.php
  
  Extract it to: sites/all/libraries/extjs/version
  
  e.g. the current (as on Feb 16, 2010) version of ExtJs should be extracted to:
  sites/all/libraries/extjs/3.1.0
  
  Please preserve the internal file organization of ExtJs files. However, you can safely delete docs and examples
  from the package.
  
  An installation video of my another open-source Drupal module, Take Control, demonstrates how you should
  extract the ExtJs javascript library to your sites/all/libraries folder. The video is available at:
  http://www.rahulsingla.com/projects/drupal-take-control-module/installation


-- INSTALLATION --

* Please read and follow the instructions below carefully.

1) Download and extract ExtJs Javascript library to /sites/all/libraries/extjs folder as described above.

2) Extract the module zip to your /sites/all/modules folder (or to your particular site folder).

3) Enable the module(s) as usual from your Drupal admin Modules section at:
  domain.com/admin/build/modules

4) Visit the module's admin section at: /admin/settings/reference_links, and change settings appropriately.


-- CONFIGURATION --

* Configure administration options in Administer >> Site Configuration >> Reference Links >>


* Configure user permissions in Administer >> User management >> Reference Links >>
  reference_links module:

  - administer reference links
  - create reference links
  - delete reference links
  - rate reference links
  
  Roles with adminster rights can configure the settings for the module in the Administration section of your portal.

  Roles with "Delete Reference Links" permission would be able to delete existing links attached to nodes.
  Please note that the link with all its associated votes would be lost, and you would no be able to recover data
  for deleted links.

  Roles with "Create Reference Links" permission would be able to add new Links for nodes.

  Roles with "Rate Reference Links" permission would be able to rate the Reference Links attached to a node.
  You can control in Administration section whether users are allowed to re-rate links they have rated previously,
  and the widget to use for Voting.


-- USING THE MODULE --

* Depending upon the content type for which you have enabled this module in the Admin section & the
  configured Permissions, you would see the links for a node that have been already created for that node,
  and a "Add New Link" option, on the View page of the node itself.
  
  You can add new link, or rate them based on your permissions. All actions are asynchronous, and do not cause
  any page refresh. 


-- CONTACT --

Current maintainers:
* Rahul Singla (r_honey) - http://drupal.org/user/473356

The module's permanent address on my website is:
http://www.rahulsingla.com/projects/drupal-reference-links-module

For paid customizations of the project, contact me through my personal website above.
