<?php

// $Id:


require_once (drupal_get_path('module', 'reference_links') . '/reference_links.inc');
require_once (drupal_get_path('module', 'reference_links') . '/reference_links.voting.inc');

function reference_links_add() {
  try {
    reference_links_headers();

    $nid = $_REQUEST['nid'];
    $title = $_REQUEST['title'];
    $url = $_REQUEST['url'];

    if (!isset($nid) || !isset($title) || !isset($url)) {
      die(t('Invalid input.'));
    }

    $result = db_query("SELECT * FROM {reference_links} WHERE url = '%s'", $url);
    if ($link = db_fetch_object($result)) {
      die(t('The specified url has already been added as a Reference link for this page.'));
    }

    db_query("INSERT INTO {reference_links} (nid, url, title, target) VALUES (%d, '%s', '%s', '_blank')", $nid, $url, $title);

    $links = reference_links_node_links($nid);
    echo json_encode($links);
  }
  catch (Exception $e) {
    die($e->getMessage() + "\n" + $e->getTraceAsString());
  }
  die();
}

function reference_links_delete() {
  try {
    reference_links_headers();

    $nid = $_REQUEST['nid'];
    $lid = $_REQUEST['lid'];

    if (!isset($nid) || !isset($lid)) {
      die(t('Invalid input.'));
    }

    reference_links_delete_votes($lid);

    db_query("DELETE FROM {reference_links} WHERE lid=%d", $lid);

    echo '[]';
  }
  catch (Exception $e) {
    die($e->getMessage() + "\n" + $e->getTraceAsString());
  }
  die();
}

function reference_links_rate() {
  global $user;
  try {
    reference_links_headers();

    $nid = $_REQUEST['nid'];
    $lid = $_REQUEST['lid'];
    $rating = $_REQUEST['rating'];

    if (!isset($nid) || !isset($lid) || !isset($rating)) {
      die(t('Invalid input.'));
    }
    else if ($user->uid == 0) {
      die(t('You need log in to be able to rate Reference links.'));
    }

    if (!variable_get('reference_links_change_rating', FALSE)) {
      $voted = reference_links_get_num_votes($lid, $user->uid);
      // If the voter has not already voted.
      if ($voted) {
        die(t('You have already voted for this link.'));
      }
    }

    switch ((int) variable_get('reference_links_rating_widget', REFERENCE_LINKS_STAR_WIDGET)) {
      case REFERENCE_LINKS_STAR_WIDGET:
        $max = (int) variable_get('reference_links_star_number', 10);
        if ($rating < 1 || $rating > $max) {
          die(t('Rating must be between 1 and ' + $max + '.'));
        }
        break;

      case REFERENCE_LINKS_UPDOWN_WIDGET:
        if ($rating != 1 && $rating != -1) {
          die(t('Invalid input.'));
        }
        if ($rating == -1 && !variable_get('reference_links_updown_allowdown', TRUE)) {
          die(t('Invalid input.'));
        }
        break;

      default:
        die(t('Invalid Rating Widget. Please contact administrator and report this error.'));
    }

    $votes[] = array(
        'content_type' => 'reference_links',
        'content_id' => $lid,
        'value_type' => 'points',
        'value' => $rating);
    votingapi_set_votes($votes);

    $links = reference_links_node_links($nid);
    echo json_encode($links);
  }
  catch (Exception $e) {
    die($e->getMessage() + "\n" + $e->getTraceAsString());
  }
  die();
}

function reference_links_headers() {
  header("Content-type: text/html");
  header("Expires: Wed, 29 Jan 1975 04:15:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-cache, must-revalidate");
  header("Pragma: no-cache");
}
