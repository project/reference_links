// $Id: 

Ext.ns('Drupal.reference_links');
var rl = Drupal.reference_links;
rl.StarWidget = 0;
rl.UpDownWidget = 1;

Ext.onReady(function() {
	// var e = document.createElement('link');
		// e.rel = 'stylesheet';
		// e.href =
		// '/rahulsingla.com/sites/all/libraries/extjs/3.1.0/resources/css/ext-all.css';
		// e.type = 'text/css';
		// e.media = 'screen';
		// var head = document.getElementsByTagName('head')[0];
		// head.appendChild(e);

		rl.settings = Drupal.settings.reference_links;
		rl.nid = rl.settings.nid;
		rl.allowRating = rl.settings.allowRating;
		rl.allowDeleting = rl.settings.allowDeleting;
		rl.basePath = Drupal.settings.basePath;

		Ext.BLANK_IMAGE_URL = rl.settings.extPath + '/resources/images/default/s.gif';
		Ext.QuickTips.init();

		rl.links = Ext.get('reference_links_container_links');
		rl.loadMask = new Ext.LoadMask(Ext.getBody(), {
			msg: 'Loading...'
		});

		var add = document.getElementById('reference_links_addnew');
		if (!Ext.isEmpty(add)) {
			rl.addWindow = new Ext.Window( {
				width: 325,
				height: 125,
				layout: 'form',
				labelAlign: 'left',
				labelWidth: 75,
				closeAction: 'hide',
				modal: true,
				maximizable: false,
				bodyStyle: 'padding: 5px',
				title: 'Add new link',
				defaults: {
					width: 200
				},
				items: [ {
					xtype: 'textfield',
					ref: 'txtTitle',
					allowBlank: false,
					fieldLabel: 'Title'
				}, {
					xtype: 'textfield',
					ref: 'txtUrl',
					allowBlank: false,
					vtype: 'url',
					vtypeText: Ext.form.VTypes.urlText,
					fieldLabel: 'Url'
				}, {
					xtype: 'panel',
					border: false,
					bodyBorder: false,
					bodyStyle: 'background-color:transparent',
					fieldLabel: '&nbsp;&nbsp;',
					labelSeparator: '',
					layout: 'column',
					items: [ {
						xtype: 'button',
						text: 'Add',
						handler: rl.btnAddClicked
					}, {
						xtype: 'label',
						html: '&nbsp;&nbsp;&nbsp;'
					}, {
						xtype: 'button',
						text: 'Cancel',
						handler: function() {
							rl.addWindow.hide();
						}
					} ]
				} ]
			});
		}

		rl.showLinks(rl.settings.data);
	});

rl.makeAjaxCall = function(path, params, callback) {
	rl.loadMask.show();

	Ext.Ajax.request( {
		url: rl.basePath + 'reference_links/' + path,
		params: params,
		method: 'POST',
		success: function(response, options) {
			rl.loadMask.hide();

			callback(response.responseText);
		}
	});
};

rl.addLinkClicked = function() {
	rl.addWindow.show();
	rl.addWindow.txtTitle.focus.defer(100, rl.addWindow.txtTitle);
	rl.addWindow.txtTitle.clearInvalid();
	rl.addWindow.txtUrl.clearInvalid();
	return (false);
};

rl.btnAddClicked = function() {
	if (!rl.addWindow.txtTitle.isValid() || !rl.addWindow.txtUrl.isValid()) {
		alert('Please correct the errors in the form before submitting it.');
		return;
	}

	rl.makeAjaxCall('add', {
		nid: rl.nid,
		title: rl.addWindow.txtTitle.getValue(),
		url: rl.addWindow.txtUrl.getValue()
	}, function(response) {
		rl.addWindow.hide();
		rl.addWindow.txtTitle.setValue('');
		rl.addWindow.txtUrl.setValue('');

		if (rl.isError(response)) {
			alert(response);
		} else {
			rl.showLinks(Ext.decode(response));
		}
	});
};

rl.showLinks = function(links) {
	var nodes = rl.links.dom.childNodes;
	for (i = nodes.length - 1; i >= 0; i--) {
		rl.links.dom.removeChild(nodes[i]);
	}

	Ext
			.each(
					links,
					function(link) {
						var parent = document.createElement('div');

						var div1 = document.createElement('div');
						div1.innerHTML = String.format(
								'<a href="{0}" target="{1}">{2}</a>&nbsp;&nbsp;&nbsp;&nbsp;',
								link.url, link.target, link.title);
						var div2 = document.createElement('div');

						parent.appendChild(div1);
						parent.appendChild(div2);

						rl.links.appendChild(parent);
						Ext.get(parent).applyStyles('clear: both');

						div1 = Ext.get(div1);
						div1.applyStyles('float: left');

						if (rl.allowDeleting) {
							var delDiv = Ext.DomHelper
									.insertBefore(div1, '<div></div>', true);
							delDiv.applyStyles('float: left');

							var delImg = Ext
									.get(Ext.DomHelper
											.insertHtml(
													'afterBegin',
													delDiv.dom,
													String
															.format(
																	'<img class="icon-delete" src="{0}" lid="{1}" ext:qtip="Delete" />',
																	Ext.BLANK_IMAGE_URL, link.lid)));
							delImg.on('click', function(e, t, o) {
								var op = confirm('Delete Link: ' + link.title + ' ?');
								if (!op) {
									return;
								}
								rl.makeAjaxCall('delete', {
									nid: rl.nid,
									lid: this.getAttribute('lid')
								}, function(response) {
									if (rl.isError(response)) {
										alert(response);
									} else {
										parent.parentNode.removeChild(parent);
									}
								});
							});
						}

						switch (rl.settings.ratingWidget) {
							case rl.StarWidget:
								if (link.rating > rl.settings.numStars) {
									// Number of Stars were decreased after votes were cast. Use
									// the maximum number of stars as per setting.
									link.rating = rl.settings.numStars;
								}
								var stars = new Ext.ux.StarRating(div2, {
									totalStars: rl.settings.numStars,
									average: link.rating - 1,
									lid: link.lid,
									linkTitle: link.title
								});

								stars.on('rate', function(o, x) {
									rl.rateLink(o.lid, x, o.linkTitle);
								});
								break;

							case rl.UpDownWidget:
								Ext.DomHelper.insertHtml('afterBegin', div2, String.format(
										'<span class="span-total-rating">(Rating: {0})</span>',
										link.rating))
								var upImg = Ext
										.get(Ext.DomHelper
												.insertHtml(
														'beforeEnd',
														div2,
														String
																.format(
																		'<img class="icon-vote-up" src="{0}" lid="{1}" linkTitle="{2}" ext:qtip="Vote Up" />',
																		Ext.BLANK_IMAGE_URL, link.lid, link.title)));
								upImg.on('click', function(e, t, o) {
									rl.rateLink(this.getAttribute('lid'), +1, this
											.getAttribute('linkTitle'));
								});

								if (rl.settings.allowDown) {
									var downImg = Ext
											.get(Ext.DomHelper
													.insertHtml(
															'beforeEnd',
															div2,
															String
																	.format(
																			'<img class="icon-vote-down" src="{0}" lid="{1}" linkTitle="{2}" ext:qtip="Vote Down" />',
																			Ext.BLANK_IMAGE_URL, link.lid, link.title)));
									downImg.on('click', function(e, t, o) {
										rl.rateLink(this.getAttribute('lid'), -1, this
												.getAttribute('linkTitle'));
									});
								}
								break;

							default:
								alert('Invalid Rating Widget. Please contact administrator and report this error.');
						}
					});
};

rl.rateLink = function(lid, rating, title) {
	if (!rl.allowRating) {
		alert('You are not authorized to rate Reference links.');
		return;
	}

	var op = confirm('Update rating ' + rating + ' for the link: ' + title + ' ?');
	if (!op) {
		return;
	}

	rl.makeAjaxCall('rate', {
		nid: rl.nid,
		lid: lid,
		rating: rating
	}, function(response) {
		if (rl.isError(response)) {
			alert(response);
		} else {
			rl.showLinks(Ext.decode(response));
		}
	});
}

rl.isError = function(response) {
	if (response.charAt(0) != '[' && response.charAt(0) != '{') {
		return (true);
	} else {
		return (false);
	}
};

Ext.namespace('Ext.ux');
Ext.ux.StarRating = function(el, config) {
	this.totalStars = 5;
	this.focusClass = '';

	if (typeof el == 'object' && !config) {
		config = el;
		el = null;
	} else {
		if (el) {
			this.primaryContainer = Ext.get(el);
			this.primaryContainer.addClass('rating');
		}
	}

	Ext.ux.StarRating.superclass.constructor.call(this, config);

	if (config) {
		if (config.totalStars) {
			this.totalStars = config.totalStars;
			delete (config.totalStars);
		}

		if (config.average) {
			this.average = config.average;
			delete (config.average);
		}
	}

	this.addEvents( {
		rate: true
	});
	if (el) {
		this.isFormField = false;
		this.createInterface();
	}
};

Ext.extend(Ext.ux.StarRating, Ext.form.Field, {
	totalStars: null,
	average: 0,
	stars: null,
	selectedValue: 0,
	valueClicked: false,
	autoSize: Ext.emptyFn,
	monitorValid: false,
	inputType: 'string',
	primaryContainer: null,

	clear: function() {
		this.stars.each(function(e, t, i) {
			t.item(i).removeClass('on');
			t.item(i).removeClass('hover');
			t.item(i).dom.firstChild.style.width = "100%";
		});
	},

	fill: function(e) {
		var el = Ext.get(e.getTarget());
		var idx = el.dom.innerHTML;
		for ( var i = 0; i < idx; i++) {
			this.stars.item(i).addClass('hover');
		}
	},

	clearAndFill: function(e) {
		if (this.valueClicked)
			return;

		this.clear();
		this.fill(e);
	},

	resetStars: function() {
		if (this.valueClicked)
			return;

		this.clear();
		if (!Ext.isEmpty(this.average)) {
			var lta = Math.floor(this.average);
			for ( var i = 0; i < lta + 1; i++)
				this.stars.item(i).addClass('on');

			var diff = (this.average - Math.floor(this.average)) * 100;
			if (diff) {
				this.stars.item(lta).dom.firstChild.style.width = diff + "%";
			}
		}

	},

	adjustRating: function(e) {
		if (!this.valueClicked) {
			var el = Ext.get(e.getTarget());
			var idx = el.dom.innerHTML;
			this.setValue(idx);
			this.fill(e);
			this.fireEvent("rate", this, idx);
		}

		this.valueClicked = !this.valueClicked;
	},

	reinitialize: function() {
		this.valueClicked = false;
		this.setValue(0);
		this.clear();
		this.resetStars();
	},

	initBehavior: function() {
		this.stars.on('mouseover', this.clearAndFill, this);
		this.stars.on('mouseout', this.resetStars, this);
		this.stars.on('focus', this.clearAndFill, this);
		this.stars.on('blur', this.resetStars, this);
		this.stars.on('click', this.adjustRating, this);
	},

	createInterface: function() {
		this.stars = new Ext.CompositeElement();
		for ( var i = 0; i < this.totalStars; i++) {
			this.stars.add(Ext.DomHelper.append(this.primaryContainer, {
				tag: 'div',
				'class': 'star',
				html: '<a href="javascript:;" title="Rate it at ' + (i + 1) + '/'
						+ this.totalStars + '">' + (i + 1) + '</a>'
			}, true));
		}

		this.initBehavior();
		this.resetStars();
	},

	onRender: function(ct, position) {
		Ext.ux.StarRating.superclass.onRender.call(this, ct, position);

		this.grow = false;

		this.el.radioClass('x-hidden');
		this.setValue(0);

		ct.addClass('rating');
		this.primaryContainer = ct;
		this.createInterface();

	},

	getName: function() {
		return this.name;
	},

	getRawValue: function() {
		return this.getValue();
	},

	getValue: function() {
		return this.selectedValue;
	},

	setValue: function(x) {
		if (x > this.totalStars)
			x = this.totalStars;

		this.selectedValue = x;
		if (this.el)
			this.el.dom.value = x;
	},

	validate: function() {
		return true;
	},

	clearInvalid: function() {
		return;
	},

	reset: function() {
		this.setValue(0);
	}
});