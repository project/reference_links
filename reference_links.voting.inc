<?php

// $Id:


require_once (drupal_get_path('module', 'reference_links') . '/reference_links.inc');

/*
 * Returns links created for a node together with its votes in descending order of Rating.
 */
function reference_links_node_links($nid) {
  $result = db_query('SELECT * FROM {reference_links} WHERE nid = %d', $nid);
  $links = array();
  while ($link = db_fetch_object($result)) {
    $links[] = array(
        'lid' => $link->lid,
        'url' => $link->url,
        'title' => $link->title,
        'target' => $link->target,
        'rating' => reference_links_get_rating($link->lid));
  }

  usort($links, "reference_links_link_comparer");

  return ($links);
}

/*
 * Compares 2 links based on their rating to sort them in descending order of their rating.
 */
function reference_links_link_comparer($a, $b) {
  return ($b['rating'] - $a['rating']);
}

/*
 * Returns the rating for the Link with the passed link id (lid).
 */
function reference_links_get_rating($lid) {
  static $function = null;
  if (is_null($function)) {
    $function = ((int) variable_get('reference_links_rating_widget', REFERENCE_LINKS_STAR_WIDGET) == REFERENCE_LINKS_STAR_WIDGET) ? 'average' : 'sum';
  }

  $criteria['content_id'] = $lid;
  $criteria['content_type'] = 'reference_links';
  $criteria['value_type'] = 'points';
  $criteria['function'] = $function;
  $results = votingapi_select_results($criteria);
  if (empty($results)) {
    return 0;
  }
  else {
    return $results[0]['value'];
  }
}

/*
 * Returns the number of votes a user has casted for a Link.
 */
function reference_links_get_num_votes($lid, $uid) {
  $criteria['content_id'] = $lid;
  $criteria['content_type'] = 'reference_links';
  $criteria['value_type'] = 'points';
  if ($uid == 0) {
    $criteria['vote_source'] = ip_address();
  }
  else {
    $criteria['uid'] = $uid;
  }
  $results = votingapi_select_votes($criteria);
  return count($results);
}

function reference_links_delete_votes($ids) {
  $criteria['content_type'] = 'reference_links';
  $criteria['content_id'] = $ids;
  $votes = votingapi_select_votes($criteria);
  votingapi_delete_votes($votes);
}
