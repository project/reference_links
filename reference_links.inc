<?php

// $Id:


define('REFERENCE_LINKS_STAR_WIDGET', 0);
define('REFERENCE_LINKS_UPDOWN_WIDGET', 1);

/*
 * Returns the base installation directory for the current website (the base physical path for the Drupal installation).
 */
function reference_links_app_dir() {
  return (dirname($_SERVER['SCRIPT_FILENAME']));
}

/*
 * Returns the base physical directory for the ExtJs javascript library.
 */
function reference_links_base_extjs_dir() {
  return (reference_links_app_dir() . str_replace('/', DIRECTORY_SEPARATOR, '/sites/all/libraries/extjs'));
}

/*
 * Returns an array of all ExtJs physical directories for its different versions available on the current Drupal installation.
 */
function reference_links_extjs_dirs() {
  $extvers = array();
  $basedir = reference_links_base_extjs_dir();
  foreach (scandir($basedir) as $item) {
    if ($item == '.' || $item == '..')
      continue;
    if (is_dir($basedir . DIRECTORY_SEPARATOR . $item)) {
      $extvers[] = $item;
    }
  }

  return ($extvers);
}

/*
 * Returns the virtual base path for the ExtJs directory from which content should be served to the client.
 */
function reference_links_ext_path() {
  $extdir = variable_get('reference_links_extjs_ver', NULL);
  if (is_null($extdir)) {
    $dirs = reference_links_extjs_dirs();
    $extdir = $dirs[0];
  }
  return ('sites/all/libraries/extjs/' . $extdir);
}

