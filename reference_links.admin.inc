<?php

// $Id:


require_once (drupal_get_path('module', 'reference_links') . '/reference_links.inc');

function reference_links_settings_form(&$form_state = array()) {
  $types = node_get_types();
  $options = array();
  $defaults = array();
  foreach ($types as $type => $info) {
    $options[$type] = $info->name;
    if (variable_get('reference_links_types_' . $type, TRUE)) {
      $defaults[] = $type;
    }
  }

  $form['reference_links_general_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => t('General settings'));

  $form['reference_links_general_fieldset']['reference_links_display_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content types'),
      '#options' => $options,
      '#default_value' => $defaults,
      '#description' => t('Choose which content types to display the Reference Links on.'));

  $form['reference_links_general_fieldset']['reference_links_teasers'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Links on teasers'),
      '#default_value' => variable_get('reference_links_teasers', FALSE),
      '#description' => t('If checked, the Reference Links will appear on teasers in lists of nodes.'));

  $form['reference_links_general_fieldset']['reference_links_collapsed'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display Collpased'),
      '#default_value' => variable_get('reference_links_collapsed', FALSE),
      '#description' => t('True to display the Reference links collapsed, false otherwise.'));

  $form['reference_links_general_fieldset']['reference_links_rating_widget'] = array(
      '#type' => 'select',
      '#title' => t('Rating Widget'),
      '#description' => t('Widget to use for the Rating form.'),
      '#required' => TRUE,
      '#default_value' => variable_get('reference_links_rating_widget', REFERENCE_LINKS_STAR_WIDGET),
      '#options' => array(
          REFERENCE_LINKS_STAR_WIDGET => t('Star Widget'),
          REFERENCE_LINKS_UPDOWN_WIDGET => t('Up/Down Widget')));

  $form['reference_links_general_fieldset']['reference_links_change_rating'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow users to change rating'),
      '#default_value' => variable_get('reference_links_change_rating', FALSE),
      '#description' => t('If checked, users would be able to change their previous rating they have given for a link any number of times.'));

  $form['reference_links_general_fieldset']['reference_links_extjs_ver'] = array(
      '#type' => 'select',
      '#options' => drupal_map_assoc(reference_links_extjs_dirs()),
      '#default_value' => variable_get('reference_links_extjs_ver', ''),
      '#required' => TRUE,
      '#title' => t('ExtJs version directory'),
      '#description' => t('ExtJs version directory (in /sites/all/libraries) that should be used by the Reference Links module.'));

  $form['reference_links_star_fieldset'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Star Rating settings'),
      '#description' => t('Settings specific to the Star Rating Widget.'));

  $form['reference_links_star_fieldset']['reference_links_star_number'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of Stars'),
      '#default_value' => variable_get('reference_links_star_number', 10),
      '#description' => t('Number of stars to display for the Star widget. Please decide this value during the intial setup of the module, if using this widget, as there is currently no support for normalizing votes that have already been cast when this value is changed.'));

  $form['reference_links_updown_fieldset'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Up/Down Rating settings'),
      '#description' => t('Settings specific to the Up/Down Rating Widget. An Up vote corresponds to +1 increase in the rating for the link, whereas a down vote degrades the rating of the link by -1.'));

  $form['reference_links_updown_fieldset']['reference_links_updown_allowdown'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow Voting Down'),
      '#default_value' => variable_get('reference_links_updown_allowdown', TRUE),
      '#description' => t('If checked users would be able to vote down a link, otherwise they would only be able to vote it up.'));

  $form['#submit'][] = 'reference_links_settings_submit_handler';

  return system_settings_form($form);
}

function reference_links_settings_form_validate($form, &$form_state) {
  $number = $form_state['values']['reference_links_star_number'];
  $number = (int) $number;
  if (!is_int($number)) {
    form_set_error('reference_links_star_number', t('Number of stars should be an integer.'));
  }
  else {
    if ($number < 1 || $number > 10) {
      form_set_error('reference_links_star_number', t('Number of stars should be between 1 and 10.'));
    }
  }
}

/**
 * Custom submit handler to process custom form data.
 */
function reference_links_settings_submit_handler($form, &$form_state) {
  $types = node_get_types();
  foreach ($types as $type => $info) {
    if (!empty($form_state['values']['reference_links_display_types'][$type])) {
      variable_set('reference_links_types_' . $type, TRUE);
    }
    else {
      variable_set('reference_links_types_' . $type, FALSE);
    }
  }

  variable_set('reference_links_teasers', $form_state['values']['reference_links_teasers']);
  variable_set('reference_links_collapsed', $form_state['values']['reference_links_collapsed']);
  variable_set('reference_links_rating_widget', $form_state['values']['reference_links_rating_widget']);
  variable_set('reference_links_change_rating', $form_state['values']['reference_links_change_rating']);
  variable_set('reference_links_extjs_ver', $form_state['values']['reference_links_extjs_ver']);

  variable_set('reference_links_star_number', (int) $form_state['values']['reference_links_star_number']);
  variable_set('reference_links_updown_allowdown', (int) $form_state['values']['reference_links_updown_allowdown']);
}
